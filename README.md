# Blackbooks-APIs

## Caractéristiques de l'API
-  Environnement : nodeJS
-  Framework : Express
-  Structure : MVC

## Installation de l'API
La branche la plus avancée est "livres". 
Pour pouvoir faire fonctionner l'API, il faut : 
-  récupérer le script sql "blackbooks.sql" et l'insérer dans un SGBDR
-  modifier les identifiants de connection dans le fichier database/connection.js 

